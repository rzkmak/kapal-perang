package com.rizkimaulana.battleship.domain;


public enum Condition {
    BLANK("_"), ALIVE("B"), DEAD("X"), MISSED("O");
    public final String label;

    Condition(String label) {
        this.label = label;
    }
}
