package com.rizkimaulana.battleship.domain;

import java.util.Arrays;
import java.util.Map;
import java.util.stream.IntStream;

public class Battlefield implements Comparable<Battlefield>{
    private int mapSize = 0;
    private Battlefield enemy;
    private Map<String, Condition> gameField;

    public Battlefield(Map<String, Condition> gameField, int mapSize) {
        this.gameField = gameField;
        this.mapSize = mapSize;
    }

    public void setEnemy(Battlefield enemy) {
        this.enemy = enemy;
    }

    public void attack(String shoot) {
        Arrays.asList(shoot.split(":"))
                .parallelStream()
                .forEach(element -> {
                    if (enemy.gameField.getOrDefault(element, Condition.BLANK) == Condition.ALIVE) {
                        enemy.gameField.replace(element, Condition.DEAD);
                    }
                    if (enemy.gameField.getOrDefault(element, Condition.BLANK) == Condition.BLANK) {
                        enemy.gameField.put(element, Condition.MISSED);
                    }
                });
    }

    public void printResultMap() {
        IntStream.rangeClosed(1, mapSize).forEachOrdered(row -> {
            IntStream.rangeClosed(1, mapSize).forEachOrdered(col -> {
                System.out.print(enemy.gameField.getOrDefault(row + "," + col, Condition.BLANK).label);
            });
            System.out.println();
        });
    }

    public int getTotalShoot() {
        return (int) this.enemy.gameField
                .entrySet()
                .stream()
                .filter(element -> element.getValue() == Condition.DEAD)
                .count();
    }

    @Override
    public int compareTo(Battlefield o) {
        return Integer.compare(this.getTotalShoot(), o.getTotalShoot());
    }
}
