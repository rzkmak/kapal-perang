package com.rizkimaulana.battleship.factory;

import com.rizkimaulana.battleship.domain.Battlefield;
import com.rizkimaulana.battleship.domain.Condition;

import java.util.Arrays;
import java.util.HashMap;
import java.util.Map;

public class BattlefieldFactory {
    public static Battlefield createNewBattlefield(String shipPosition, int mapSize) {
        Map<String, Condition> map = new HashMap<>();
        Arrays.asList(shipPosition.split(":"))
                .parallelStream()
                .forEach(element -> {
                    map.put(element, Condition.ALIVE);
                });
        return new Battlefield(map, mapSize);
    }
}