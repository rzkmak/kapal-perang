package com.rizkimaulana.battleship;

import com.rizkimaulana.battleship.domain.Battlefield;
import com.rizkimaulana.battleship.factory.BattlefieldFactory;

public class MainApplication {
    public static void main(String[] args) {
        int mapSize = 5;
        String shipPositionP1 = "1,1:1,2:3,3:4,4";
        String shootP1 = "1,1:1,2:3,3:4,4";
        String shipPositionP2 = "1,1:1,2:3,3:4,4";
        String shootP2 = "1,3:1,2:3,3:4,4";

        Battlefield player1 = BattlefieldFactory.createNewBattlefield(shipPositionP1, mapSize);
        Battlefield player2 = BattlefieldFactory.createNewBattlefield(shipPositionP2, mapSize);

        player1.setEnemy(player2);
        player2.setEnemy(player1);
        player1.attack(shootP1);
        player2.attack(shootP2);

        player1.printResultMap();
        player2.printResultMap();

        System.out.println(String.format("P1: %s", player1.getTotalShoot()));
        System.out.println(String.format("P2: %s", player2.getTotalShoot()));

        String result;

        if (player1.compareTo(player2) == 0) {
            result = "It is a draw";
        } else if (player1.compareTo(player2) > 0) {
            result = "P1 wins";
        } else {
            result = "P2 wins";
        }

        System.out.println(result);
    }
}
